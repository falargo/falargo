# Falargo - The Fastest Language in Existence

**DISCLAIMER:** To prove the authenticity of any document, check the signatures on it. 
If the file isn't signed by the keys 0x0E4E01654AF248AA (jeudemots) or 0xFD106772C41B628E (rdenf) it should be considered unauthentic.
Check the file roster for key fingerprints.

If you can't find a signature on file.txt itself, check the directory for a file.txt.sig file.


